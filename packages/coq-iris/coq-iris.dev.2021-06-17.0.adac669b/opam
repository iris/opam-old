opam-version: "2.0"
maintainer: "Ralf Jung <jung@mpi-sws.org>"
authors: "The Iris Team"
license: "BSD-3-Clause"
homepage: "https://iris-project.org/"
bug-reports: "https://gitlab.mpi-sws.org/iris/iris/issues"
dev-repo: "git+https://gitlab.mpi-sws.org/iris/iris.git"
version: "dev"

synopsis: "A Higher-Order Concurrent Separation Logic Framework with support for interactive proofs"
description: """
This package provides the following Coq modules:
iris.prelude, iris.algebra, iris.si_logic, iris.bi, iris.proofmode, iris.base_logic, iris.program_logic.
"""

depends: [
  "coq" { (>= "8.12" & < "8.14~") | (= "dev") }
  "coq-stdpp" { (= "dev.2021-06-16.0.fc5f75e5") | (= "dev") }
]

build: ["./make-package" "iris" "-j%{jobs}%"]
install: ["./make-package" "iris" "install"]
url { src: "git+https://gitlab.mpi-sws.org/iris/iris.git#adac669b3a677c7ab5620d218a8cf245e2accb95" }
