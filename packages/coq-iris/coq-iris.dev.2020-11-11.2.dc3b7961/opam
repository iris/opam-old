opam-version: "2.0"
maintainer: "Ralf Jung <jung@mpi-sws.org>"
authors: "The Iris Team"
license: "BSD-3-Clause"
homepage: "https://iris-project.org/"
bug-reports: "https://gitlab.mpi-sws.org/iris/iris/issues"
dev-repo: "git+https://gitlab.mpi-sws.org/iris/iris.git"

synopsis: "Iris is a Higher-Order Concurrent Separation Logic Framework with support for interactive proofs"
description: """
This package provides the following Coq modules:
iris.prelude, iris.algebra, iris.si_logic, iris.bi, iris.proofmode, iris.base_logic, iris.program_logic.
"""

depends: [
  "coq" { (>= "8.10.2" & < "8.13~") | (= "dev") }
  "coq-stdpp" { (= "dev.2020-10-30.0.402f2d6a") | (= "dev") }
]

build: ["./make-package" "iris" "-j%{jobs}%"]
install: ["./make-package" "iris" "install"]
url { src: "git+https://gitlab.mpi-sws.org/iris/iris.git#dc3b796197402f31c1f405fd9bf9677593b8b069" }
