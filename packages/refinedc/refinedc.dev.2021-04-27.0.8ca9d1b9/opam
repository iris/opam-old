opam-version: "2.0"
name: "refinedc"
synopsis: "RefinedC verification framework"
description: """
RefinedC is a framework for verifying idiomatic, low-level C code using a
combination of refinement types and ownership types.
"""
license: "BSD"

maintainer: ["Michael Sammler <msammler@mpi-sws.org>"
             "Rodolphe Lepigre <lepigre@mpi-sws.org>"]
authors: ["Michael Sammler" "Rodolphe Lepigre" "Kayvan Memarian"]

homepage: "https://plv.mpi-sws.org/refinedc"
bug-reports: "https://gitlab.mpi-sws.org/iris/refinedc/issues"
dev-repo: "git+https://gitlab.mpi-sws.org/iris/refinedc.git"

depends: [
  "coq" { (>= "8.12.0" & < "8.13~") }
  "coq-iris" { (= "dev.2021-04-22.0.dea10729") | (= "dev") }
  "dune" {>= "2.7.0"}
  "cerberus" {= "~dev"}
  "cmdliner" {>= "1.0.4"}
  "earley" {= "3.0.0"}
  "toml" {= "5.0.0"}
]

build: [
  ["dune" "subst"] {pinned}
  ["dune" "build" "-p" name "-j" jobs]
]
url { src: "git+https://gitlab.mpi-sws.org/iris/refinedc.git#8ca9d1b9735bec4c269957f758bb688cbae77c0c" }
